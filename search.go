package jikan

import (
	"fmt"
	"time"
)

// SearchMangaResult is the individual data returned by the site's
// search when looking up manga
type SearchMangaResult struct {
	MalID      int       `json:"mal_id"`
	URL        string    `json:"url"`
	ImageURL   string    `json:"image_url"`
	Title      string    `json:"title"`
	Publishing bool      `json:"publishing"`
	Synopsis   string    `json:"synopsis"`
	Type       string    `json:"type"`
	Chapters   int       `json:"chapters"`
	Volumes    int       `json:"volumes"`
	Score      float64   `json:"score"`
	StartDate  time.Time `json:"start_date"`
	EndDate    time.Time `json:"end_date"`
	Members    int       `json:"members"`
}

// SearchMangaResults contains all the results of a manga search
type SearchMangaResults struct {
	RequestHash        string              `json:"request_hash"`
	RequestCached      bool                `json:"request_cached"`
	RequestCacheExpiry int                 `json:"request_cache_expiry"`
	Results            []SearchMangaResult `json:"results"`
	LastPage           int                 `json:"last_page"`
}

// SearchManga uses given query to search for manga on the given page.
// Optional requests allow for a more fine-grained search. Returns
// a SearchMangaResults object and an error.
func SearchManga(query string, page int, requests ...string) (SearchMangaResults, error) {
	var mangaresults SearchMangaResults
	err := apiSearchGet(query, "manga", page, &mangaresults, requests...)
	if err != nil {
		return mangaresults, fmt.Errorf("failed searching manga: %s", err)
	}

	return mangaresults, nil
}

// SearchAnimeResult is the individual data returned by the site's
// search when looking up anime
type SearchAnimeResult struct {
	MalID     int       `json:"mal_id"`
	URL       string    `json:"url"`
	ImageURL  string    `json:"image_url"`
	Title     string    `json:"title"`
	Airing    bool      `json:"airing"`
	Synopsis  string    `json:"synopsis"`
	Type      string    `json:"type"`
	Episodes  int       `json:"episodes"`
	Score     float64   `json:"score"`
	StartDate time.Time `json:"start_date"`
	EndDate   time.Time `json:"end_date"`
	Members   int       `json:"members"`
	Rated     string    `json:"rated"`
}

// SearchAnimeResults contains all the results of an anime search
type SearchAnimeResults struct {
	RequestHash        string              `json:"request_hash"`
	RequestCached      bool                `json:"request_cached"`
	RequestCacheExpiry int                 `json:"request_cache_expiry"`
	Results            []SearchAnimeResult `json:"results"`
	LastPage           int                 `json:"last_page"`
}

// SearchAnime uses given query to search for anime on the given page.
// Optional requests allow for a more fine-grained search. Returns
// a SearchAnimeResults object and an error.
func SearchAnime(query string, page int, requests ...string) (SearchAnimeResults, error) {
	var animeresults SearchAnimeResults
	err := apiSearchGet(query, "anime", page, &animeresults, requests...)
	if err != nil {
		return animeresults, fmt.Errorf("failed searching anime: %s", err)
	}

	return animeresults, nil
}
