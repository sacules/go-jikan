package jikan

import (
	"testing"
)

func TestGetMangaInfo(t *testing.T) {
	manga, err := GetMangaInfo(436)
	if err != nil {
		t.Errorf("error getting uzumaki: %s", err)
	}

	if manga.MalID != 436 {
		t.Error("got ID", manga.MalID, "and expected", 436)
	}

	if manga.Title != "Uzumaki" {
		t.Error("got title", manga.Title, "and expected", "Uzumaki")
	}

	if manga.TitleJapanese != "うずまき" {
		t.Error("got japanese title", manga.TitleJapanese, "and expected", "うずまき")
	}
}
