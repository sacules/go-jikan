package jikan

import (
	"time"
)

// MangaInfo is the JSON returned by Jikan's REST API when requesting info
// about a particular manga given its ID.
type MangaInfo struct {
	RequestHash        string        `json:"request_hash"`
	RequestCached      bool          `json:"request_cached"`
	RequestCacheExpiry int           `json:"request_cache_expiry"`
	MalID              int           `json:"mal_id"`
	URL                string        `json:"url"`
	Title              string        `json:"title"`
	TitleEnglish       string        `json:"title_english"`
	TitleSynonyms      []string      `json:"title_synonyms"`
	TitleJapanese      string        `json:"title_japanese"`
	Status             string        `json:"status"`
	ImageURL           string        `json:"image_url"`
	Type               string        `json:"type"`
	Volumes            int           `json:"volumes"`
	Chapters           int           `json:"chapters"`
	Publishing         bool          `json:"publishing"`
	Published          Published     `json:"published"`
	Rank               int           `json:"rank"`
	Score              float64       `json:"score"`
	ScoredBy           int           `json:"scored_by"`
	Popularity         int           `json:"popularity"`
	Members            int           `json:"members"`
	Favorites          int           `json:"favorites"`
	Synopsis           string        `json:"synopsis"`
	Background         string        `json:"background"`
	Related            Related       `json:"related"`
	Genres             []SiteContent `json:"genres"`
	Authors            []SiteContent `json:"authors"`
	Serializations     []SiteContent `json:"serializations"`
}

// GetMangaInfo returns general information about the given manga, and
// an error if it couldn't be found.
func GetMangaInfo(id int) (MangaInfo, error) {
	var minfo MangaInfo
	err := apiIDGet(id, "manga", "", 0, &minfo)
	return minfo, err
}

// MangaCharacter contains information about a particular manga character.
type MangaCharacter struct {
	MalID    int    `json:"mal_id"`
	URL      string `json:"url"`
	ImageURL string `json:"image_url"`
	Name     string `json:"name"`
	Role     string `json:"role"`
}

// MangaCharacters contains the list of characters in manga
type MangaCharacters struct {
	RequestHash        string           `json:"request_hash"`
	RequestCached      bool             `json:"request_cached"`
	RequestCacheExpiry int              `json:"request_cache_expiry"`
	Characters         []MangaCharacter `json:"characters"`
}

// GetMangaCharacters fetches the list of characters in the given manga.
// Returns an error if it couldn't be found.
func GetMangaCharacters(id int) (MangaCharacters, error) {
	var mchars MangaCharacters
	err := apiIDGet(id, "manga", "characters", 0, &mchars)
	return mchars, err
}

// MangaStats contains statistical information about a manga
type MangaStats struct {
	RequestHash        string        `json:"request_hash"`
	RequestCached      bool          `json:"request_cached"`
	RequestCacheExpiry int           `json:"request_cache_expiry"`
	Reading            int           `json:"reading"`
	Completed          int           `json:"completed"`
	OnHold             int           `json:"on_hold"`
	Dropped            int           `json:"dropped"`
	PlanToRead         int           `json:"plan_to_read"`
	Total              int           `json:"total"`
	Scores             map[int]Score `json:"scores"`
}

// GetMangaStats fetches statistics about the given manga.
// Returns an error if it couldn't be found.
func GetMangaStats(id int) (MangaStats, error) {
	var mstats MangaStats
	err := apiIDGet(id, "manga", "stats", 0, &mstats)
	return mstats, err
}

// MangaReviewScore contains the different metrics used while
// reviewing and giving a score to a manga.
type MangaReviewScore struct {
	Overall   int `json:"overall"`
	Story     int `json:"story"`
	Art       int `json:"art"`
	Character int `json:"character"`
	Enjoyment int `json:"enjoyment"`
}

// MangaReviewer contains information about the user reviewing
// a manga.
type MangaReviewer struct {
	URL          string           `json:"url"`
	ImageURL     string           `json:"image_url"`
	Username     string           `json:"username"`
	ChaptersRead int              `json:"chapters_read"`
	Scores       MangaReviewScore `json:"scores"`
}

// MangaReview contains a full review of a manga.
type MangaReview struct {
	MalID        int           `json:"mal_id"`
	URL          string        `json:"url"`
	HelpfulCount int           `json:"helpful_count"`
	Date         time.Time     `json:"date"`
	Reviewer     MangaReviewer `json:"reviewer"`
	Content      string        `json:"content"`
}

// MangaReviews contains a single page of manga reviews
type MangaReviews struct {
	RequestHash        string        `json:"request_hash"`
	RequestCached      bool          `json:"request_cached"`
	RequestCacheExpiry int           `json:"request_cache_expiry"`
	Reviews            []MangaReview `json:"reviews"`
}

// GetMangaReviews fetches the reviews for the given manga written by
// users of the site on the given page. Returns an error if it
// couldn't be found.
func GetMangaReviews(id, page int) (MangaReviews, error) {
	var mreviews MangaReviews
	err := apiIDGet(id, "manga", "reviews", page, &mreviews)
	return mreviews, err
}

// MangaUserUpdate contains info about the progress of a user on a manga
type MangaUserUpdate struct {
	Username      string    `json:"username"`
	URL           string    `json:"url"`
	ImageURL      string    `json:"image_url"`
	Score         int       `json:"score"`
	Status        string    `json:"status"`
	VolumesRead   int       `json:"volumes_read"`
	VolumesTotal  int       `json:"volumes_total"`
	ChaptersRead  int       `json:"chapters_read"`
	ChaptersTotal int       `json:"chapters_total"`
	Date          time.Time `json:"date"`
}

// MangaUserUpdates contains a single page of user updates to a manga.
type MangaUserUpdates struct {
	RequestHash        string            `json:"request_hash"`
	RequestCached      bool              `json:"request_cached"`
	RequestCacheExpiry int               `json:"request_cache_expiry"`
	Users              []MangaUserUpdate `json:"users"`
}

// GetMangaUserUpdates fetches the latest user updates to a manga on
// the given page.
func GetMangaUserUpdates(id, page int) (MangaUserUpdates, error) {
	var muserupdates MangaUserUpdates
	err := apiIDGet(id, "manga", "userupdates", page, &muserupdates)
	return muserupdates, err
}
