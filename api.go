package jikan

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

const (
	// APIURL is the base URL of Jikan's REST API
	APIURL = "https://api.jikan.moe/v3/"
)

func apiIDGet(id int, object, request string, page int, data interface{}) error {
	url := APIURL + object + "/" + strconv.Itoa(id)

	if request != "" {
		url += "/" + request

		if page != 0 {
			url += "/" + strconv.Itoa(page)
		}
	}

	return getAndDecode(url, data)
}

func apiSearchGet(query, object string, page int, data interface{}, requests ...string) error {
	// Format query correctly
	query = strings.Replace(query, " ", "+", -1)

	url := APIURL + "search/" + object + "?q=" + query

	// Optional parameters
	for _, r := range requests {
		url += "&" + r
	}

	url += "&page=" + strconv.Itoa(page)

	return getAndDecode(url, data)
}

// Small generic helper function to unmarshal responses into the provided data
func getAndDecode(url string, data interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("error fetching url %s: %s", url, err)
	}
	defer resp.Body.Close()

	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(data)
	if err != nil {
		return fmt.Errorf("decoding response: %s", err)
	}

	return nil
}
