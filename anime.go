package jikan

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

// AnimeInfo is the JSON returned by Jikan's REST API when requesting info
// about a particular anime given its ID.
type AnimeInfo struct {
	RequestHash        string   `json:"request_hash"`
	RequestCached      bool     `json:"request_cached"`
	RequestCacheExpiry int      `json:"request_cache_expiry"`
	MalID              int      `json:"mal_id"`
	URL                string   `json:"url"`
	ImageURL           string   `json:"image_url"`
	TrailerURL         string   `json:"trailer_url"`
	Title              string   `json:"title"`
	TitleEnglish       string   `json:"title_english"`
	TitleJapanese      string   `json:"title_japanese"`
	TitleSynonyms      []string `json:"title_synonyms"`
	Type               string   `json:"type"`
	Source             string   `json:"source"`
	Episodes           int      `json:"episodes"`
	Status             string   `json:"status"`
	Airing             bool     `json:"airing"`
	Aired              struct {
		From time.Time `json:"from"`
		To   time.Time `json:"to"`
		Prop struct {
			From struct {
				Day   int `json:"day"`
				Month int `json:"month"`
				Year  int `json:"year"`
			} `json:"from"`
			To struct {
				Day   int `json:"day"`
				Month int `json:"month"`
				Year  int `json:"year"`
			} `json:"to"`
		} `json:"prop"`
		String string `json:"string"`
	} `json:"aired"`
	Duration   string  `json:"duration"`
	Rating     string  `json:"rating"`
	Score      float64 `json:"score"`
	ScoredBy   int     `json:"scored_by"`
	Rank       int     `json:"rank"`
	Popularity int     `json:"popularity"`
	Members    int     `json:"members"`
	Favorites  int     `json:"favorites"`
	Synopsis   string  `json:"synopsis"`
	Background string  `json:"background"`
	Premiered  string  `json:"premiered"`
	Broadcast  string  `json:"broadcast"`
	Related    struct {
		Adaptation []struct {
			MalID int    `json:"mal_id"`
			Type  string `json:"type"`
			Name  string `json:"name"`
			URL   string `json:"url"`
		} `json:"Adaptation"`
		AlternativeVersion []struct {
			MalID int    `json:"mal_id"`
			Type  string `json:"type"`
			Name  string `json:"name"`
			URL   string `json:"url"`
		} `json:"Alternative version"`
		SideStory []struct {
			MalID int    `json:"mal_id"`
			Type  string `json:"type"`
			Name  string `json:"name"`
			URL   string `json:"url"`
		} `json:"Side story"`
		SpinOff []struct {
			MalID int    `json:"mal_id"`
			Type  string `json:"type"`
			Name  string `json:"name"`
			URL   string `json:"url"`
		} `json:"Spin-off"`
	} `json:"related"`
	Producers []struct {
		MalID int    `json:"mal_id"`
		Type  string `json:"type"`
		Name  string `json:"name"`
		URL   string `json:"url"`
	} `json:"producers"`
	Licensors []struct {
		MalID int    `json:"mal_id"`
		Type  string `json:"type"`
		Name  string `json:"name"`
		URL   string `json:"url"`
	} `json:"licensors"`
	Studios []struct {
		MalID int    `json:"mal_id"`
		Type  string `json:"type"`
		Name  string `json:"name"`
		URL   string `json:"url"`
	} `json:"studios"`
	Genres []struct {
		MalID int    `json:"mal_id"`
		Type  string `json:"type"`
		Name  string `json:"name"`
		URL   string `json:"url"`
	} `json:"genres"`
	OpeningThemes []string `json:"opening_themes"`
	EndingThemes  []string `json:"ending_themes"`
}

// GetAnimeInfo lol
func GetAnimeInfo(id int) (*AnimeInfo, error) {
	resp, err := http.Get(APIURL + "anime/" + strconv.Itoa(id))
	if err != nil {
		return nil, fmt.Errorf("error getting manga %d: %s", id, err)
	}
	defer resp.Body.Close()

	m := new(AnimeInfo)
	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(m)
	if err != nil {
		return nil, err
	}

	return m, nil
}
