package jikan

import (
	"time"
)

type SiteContent struct {
	MalID int    `json:"mal_id"`
	Type  string `json:"type"`
	Name  string `json:"name"`
	URL   string `json:"url"`
}

type Related struct {
	Adaptation         []SiteContent `json:"Adaptation"`
	AlternativeVersion []SiteContent `json:"Alternative version"`
	Character          []SiteContent `json:"Character"`
	Other              []SiteContent `json:"Other"`
	Prequel            []SiteContent `json:"Prequel"`
	Sequel             []SiteContent `json:"Sequel"`
	SideStory          []SiteContent `json:"Side story"`
	SpinOff            []SiteContent `json:"Spin-off"`
	Summary            []SiteContent `json:"Summary"`
}

type Date struct {
	Day   int `json:"day"`
	Month int `json:"month"`
	Year  int `json:"year"`
}

type Prop struct {
	From Date `json:"from"`
	To   Date `json:"to"`
}

type Published struct {
	From   time.Time `json:"from"`
	To     time.Time `json:"to"`
	Prop   Prop      `json:"prop"`
	String string    `json:"string"`
}

type Article struct {
	URL        string    `json:"url"`
	Title      string    `json:"title"`
	Date       time.Time `json:"date"`
	AuthorName string    `json:"author_name"`
	AuthorURL  string    `json:"author_url"`
	ForumURL   string    `json:"forum_url"`
	ImageURL   string    `json:"image_url"`
	Comments   int       `json:"comments"`
	Intro      string    `json:"intro"`
}

type News struct {
	RequestHash        string    `json:"request_hash"`
	RequestCached      bool      `json:"request_cached"`
	RequestCacheExpiry int       `json:"request_cache_expiry"`
	Articles           []Article `json:"articles"`
}

type Picture struct {
	Large string `json:"large"`
	Small string `json:"small"`
}

type Pictures struct {
	RequestHash        string    `json:"request_hash"`
	RequestCached      bool      `json:"request_cached"`
	RequestCacheExpiry int       `json:"request_cache_expiry"`
	Pictures           []Picture `json:"pictures"`
}

type Score struct {
	Votes      int     `json:"votes"`
	Percentage float64 `json:"percentage"`
}

type LastPost struct {
	URL        string    `json:"url"`
	AuthorName string    `json:"author_name"`
	AuthorURL  string    `json:"author_url"`
	DatePosted time.Time `json:"date_posted"`
}

type Topic struct {
	TopicID    int       `json:"topic_id"`
	URL        string    `json:"url"`
	Title      string    `json:"title"`
	DatePosted time.Time `json:"date_posted"`
	AuthorName string    `json:"author_name"`
	AuthorURL  string    `json:"author_url"`
	Replies    int       `json:"replies"`
	LastPost   LastPost  `json:"last_post"`
}

type Forum struct {
	RequestHash        string  `json:"request_hash"`
	RequestCached      bool    `json:"request_cached"`
	RequestCacheExpiry int     `json:"request_cache_expiry"`
	Topics             []Topic `json:"topics"`
}

type MoreInfo struct {
	RequestHash        string `json:"request_hash"`
	RequestCached      bool   `json:"request_cached"`
	RequestCacheExpiry int    `json:"request_cache_expiry"`
	Moreinfo           string `json:"moreinfo"`
}

type Recommendation struct {
	MalID               int    `json:"mal_id"`
	URL                 string `json:"url"`
	ImageURL            string `json:"image_url"`
	RecommendationURL   string `json:"recommendation_url"`
	Title               string `json:"title"`
	RecommendationCount int    `json:"recommendation_count"`
}

type Recommendations struct {
	RequestHash        string           `json:"request_hash"`
	RequestCached      bool             `json:"request_cached"`
	RequestCacheExpiry int              `json:"request_cache_expiry"`
	Recommendations    []Recommendation `json:"recommendations"`
}
